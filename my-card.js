
const template1 = document.createElement('template');
template1.innerHTML=`
<style>
@import 'stylemycard.scss';
</style>
<div class="my-card">
 <p class="user-name"><slot name="name"></p>
 <p class="user-details"><slot name="details"></p>
</div>`


class MyCard extends HTMLElement
{
    constructor()
    {
        super();
        this.attachShadow({mode:'open'});
        this.shadowRoot.appendChild(template1.content.cloneNode(true));
    }
}
window.customElements.define("my-card",MyCard);
