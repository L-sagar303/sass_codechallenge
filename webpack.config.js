const path = require('path');
const HtmlWebpackPlugin = require("html-webpack-plugin");

module.exports ={
    entry:"./main.js",
    output:{
        path:path.resolve(__dirname,'dist'),
        filename:"bundle.js"
    },
    module:{
        rules: [
            {
                test:/\.scss$/,
                use:["style-loader","css-loader",{
                   loader:"sass-loader",
                options: {
                    sassOptions:{
                        includePaths: [path.resolve(__dirname,'node-modules')]
                    }
                }}]
            }
        ]
    },
    plugins:[
        new HtmlWebpackPlugin({
            template:'./index.html'
        }) 
    ],
    mode:"development",
    resolve:{
        extensions:[".scss",".js"]
    },
    devServer:{
        port:7070
    }
}